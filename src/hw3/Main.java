package hw3;

import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work.";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses.";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "watch a film.";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to the library.";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "watch a theatre.";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to the party.";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "go to the gym.";
        String modifyInput = "";
        boolean logicalFlag = false;
        do {System.out.println("Please, input the day of the week:");
            if (scanner.hasNextLine()) modifyInput = scanner.nextLine().trim().toLowerCase();
            if (modifyInput.contains("change") || modifyInput.contains("reschedule")){
                changeUserData(schedule, modifyInput );
                continue;
            }
            switch (modifyInput) {
                case "sunday":
                    printUserData(schedule, 1);
                    break;
                case "monday":
                    printUserData(schedule, 2);
                    break;
                case "tuesday":
                    printUserData(schedule, 3);
                    break;
                case "wednesday":
                    printUserData(schedule, 4);
                    break;
                case "thursday":
                    printUserData(schedule, 5);
                    break;
                case "friday":
                    printUserData(schedule, 6);
                    break;
                case "saturday":
                    printUserData(schedule, 7);
                    break;
                case "exit":
                    logicalFlag = true;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        } while (!logicalFlag);
    }
    public static void printUserData( String [][] arr, int day){
        System.out.println("Your tasks for " + arr[day - 1][0] + ": " + arr[day - 1][1]);
    }
    public static void changeUserData( String [][] arr, String modifyInput){
        Scanner scanner = new Scanner(System.in);
        String str = modifyInput.replace("change", "").replace("reschedule", "").trim();
        int counter = -1;
        for (int i = 0; i < 7; i++){
            if (arr[i][0].equalsIgnoreCase(str)) counter = i;
        }
        if (counter == -1) {
            System.out.println("Sorry, I don't understand you, please enter the day name correctly.");
        } else {
            System.out.println("Please, input new tasks for " + arr[counter][0]);
            if (scanner.hasNextLine()) arr[counter][1] = scanner.nextLine();
        }
    }
}
