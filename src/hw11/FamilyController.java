package hw11;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class FamilyController {
    Scanner scanner = new Scanner(System.in);
    FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public ArrayList<Family> getAllFamilies() {
        return this.familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        this.familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        System.out.println("Families, which consist from more then " + number + " persons");
        this.familyService.getFamiliesBiggerThan(number).forEach(e -> e.prettyFormat());
        return this.familyService.getFamiliesBiggerThan(number);
    }

    public List<Family> getFamiliesLessThan(int number) {
        System.out.println("Families, which consist from less then " + number + " persons");
        this.familyService.getFamiliesLessThan(number).forEach(e -> e.prettyFormat());;
        return this.familyService.getFamiliesLessThan(number);
    }

    public int countFamiliesWithMemberNumber(int number) {
        return this.familyService.countFamiliesWithMemberNumber(number);
    }

    public void createNewFamily(Human father, Human mother) {
        this.familyService.createNewFamily(father, mother);
    }

    public void deleteFamilyByIndex(int index) {
        this.familyService.deleteFamilyByIndex(index);
    }

    public void bornChild(Family currentFamily, String manName, String womanName) {
        if (currentFamily.countFamily() == 5) throw new FamilyOverflowException("Увага! Родина не може складатися більш ніж з 5 осіб!");
        this.familyService.bornChild(currentFamily, manName, womanName);
    }

    public Family adoptChild(Family currentFamily, Human adoptedChild) {
        if (currentFamily.countFamily() == 5) throw new FamilyOverflowException("Увага! Родина не може складатися більш ніж з 5 осіб!");
        return this.familyService.adoptChild(currentFamily, adoptedChild);
    }

    public void deleteAllChildrenOlderThen(int age) {
        this.familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return this.familyService.count();
    }

    public Family getFamilyById(int index) {
        if (getAllFamilies().size()-1 < index) throw new InputMismatchException();
        return this.familyService.getFamilyById(index);
    }

    public HashSet<Pet> getPets(int index) {
        return this.familyService.getPets(index);
    }

    public void addPet(int familyIndex, Pet pet) {
        this.familyService.addPet(familyIndex, pet);
    }
    public void setFamilyList(ArrayList<Family> familyList) {this.familyService.setFamilyList(familyList);}
    public int integerInput (String theQuest){System.out.println(theQuest);
        while(true) {
            System.out.print("Your option is: ");
            if (!scanner.hasNextInt()) {System.out.println("You should enter the option with an integer");
                scanner.next();
             continue;
            } else {return scanner.nextInt();}}}
    public String lineInput (String theQuest){
        System.out.println(theQuest);
        String string ="";
        System.out.print("Your option is: ");
        while(string.length() == 0) {string =scanner.next();}
            return string;
            }
    public Human makingHuman (String gender){
        String name = lineInput ("Ім'я");
        String surName = lineInput ("Прізвище");
        System.out.println("Дата народження:");
        String day = lineInput ("День у форматі ДД");
        String month = lineInput ("Місяць у форматі ММ");
        String year = lineInput ("Рік у форматі РРРР");
        int iq = integerInput("Введіть коефіцієнт IQ");
        if (gender=="Man"){return new Man(name, surName, day+"/"+month+"/"+year, iq);
        } else {return new Woman(name, surName, day+"/"+month+"/"+year, iq); }}
    public void menu() {

        System.out.println("Greetings, to continue, choose the option from the list below: ");

        String menu = """
               \n- 1. Заповнити тестовими даними
               - 2. Відобразити весь список сімей
               - 3. Відобразити список сімей, де кількість людей більша за задану
               - 4. Відобразити список сімей, де кількість людей менша за задану
               - 5. Підрахувати кількість сімей, де кількість членів дорівнює
               - 6. Створити нову родину
               - 7. Видалити сім'ю за індексом сім'ї у загальному списку
               - 8. Редагувати сім'ю за індексом сім'ї у загальному списку
               - 9. Видалити всіх дітей старше віку
               - 0. Exit the App.
                """;
        String subMenu = """
               - 1. Народити дитину
               - 2. Усиновити дитину
               - 3. Повернутися до головного меню
                """;
        String genderSubMenu = """
               - 1. Всиновити хлопця
               - 2. Вдочерити дівчину
               - 3. Повернутися до головного меню
                """;
        ArrayList<Family> famList = new ArrayList<>();
        while (true){
            try {
                System.out.println(menu);
                switch (integerInput("")) {
                    case 1:
                        HashMap <DayOfWeek, String> schedule1 = new HashMap<DayOfWeek, String>();
                        HashMap <DayOfWeek, String> schedule2 = new HashMap<DayOfWeek, String>();
                        HashMap <DayOfWeek, String> schedule3 = new HashMap<DayOfWeek, String>();
                        HashMap <DayOfWeek, String> schedule4 = new HashMap<DayOfWeek, String>();
                        HashMap <DayOfWeek, String> schedule5 = new HashMap<DayOfWeek, String>();
                        HashMap <DayOfWeek, String> schedule6 = new HashMap<DayOfWeek, String>();
                        HashMap <DayOfWeek, String> schedule7 = new HashMap<DayOfWeek, String>();
                        HashMap <DayOfWeek, String> schedule8 = new HashMap<DayOfWeek, String>();
                        HashMap <DayOfWeek, String> schedule9 = new HashMap<DayOfWeek, String>();
                        schedule1.put(DayOfWeek.MONDAY, "task1");
                        schedule2.put(DayOfWeek.TUESDAY, "task2");
                        schedule3.put(DayOfWeek.WEDNESDAY, "task3");
                        schedule4.put(DayOfWeek.THURSDAY, "task4");
                        schedule5.put(DayOfWeek.FRIDAY, "task5");
                        schedule6.put(DayOfWeek.SATURDAY, "task6");
                        schedule7.put(DayOfWeek.SUNDAY, "task7");
                        schedule8.put(DayOfWeek.MONDAY, "task8");
                        schedule9.put(DayOfWeek.TUESDAY, "task9");
                        Man father1 = new Man("Bohdan", "Ivanenko", "01/01/1980", 82, schedule1);
                        Man father2 = new Man("Semen", "Petrenko", "01/01/1978", 89, schedule2);
                        Man father3 = new Man("Stepan", "Sydorenko", "01/01/1976", 85, schedule3);
                        Woman mother1 = new Woman("Maria", "Ivanenko", "01/01/1982", 75, schedule4);
                        Woman mother2 = new Woman("Kateryna", "Petrenko", "01/01/1980", 91, schedule5);
                        Woman mother3 = new Woman("Galyna", "Sydorenko", "01/01/1978", 83, schedule6);
                        Man children1 = new Man("Taras", "Ivanenko", "01/01/2003", 82, schedule7);
                        Man children2 = new Man("Stepan", "Petrenko", "01/01/2007", 89, schedule8);
                        Woman children3 = new Woman("Krystyna", "Sydorenko", "01/01/1999", 82, schedule9);
                        Dog pet1 = new Dog("Sharick", 8, 48, new HashSet<>  (Set.of("play with ball", "eat meat")) );
                        Fish pet2 = new Fish( "Goldy", 2, 7, new HashSet<>  (Set.of("say 'Booolb'", "looking TV-set in front of" )));
                        Cat pet3 = new Cat( "Barsick", 6, 56, new HashSet<>  (Set.of("say 'Meow'", "eat fish")));
                        Dog pet4 = new Dog("Barbos", 7, 55, new HashSet<>  (Set.of("play with cat", "eat bones")) );
                        Family family1 = new Family( mother1,father1,new ArrayList<Human>(Set.of(children1)), new HashSet<Pet>(Set.of(pet1)));
                        Family family2 = new Family( mother2,father2,new ArrayList<Human>(Set.of(children2)), new HashSet<Pet>(Set.of(pet2)));
                        Family family3 = new Family( mother3,father3,new ArrayList<Human>(Set.of(children3)), new HashSet<Pet>(Set.of(pet3, pet4)));
                        famList.add(family1);
                        famList.add(family2);
                        famList.add(family3);
                        setFamilyList(famList);
                        break;
                    case 2:
                        this.displayAllFamilies();
                        break;
                    case 3:
                        getFamiliesBiggerThan(integerInput("Введіть цікавляче число"));
                        break;
                    case 4:
                        getFamiliesLessThan(integerInput("Введіть цікавляче число"));
                        break;
                    case 5:
                        System.out.println("Таких родин: "+ countFamiliesWithMemberNumber(integerInput("Введіть цікавляче число")));
                        break;
                    case 6:
                        System.out.println("Створення нової родини.");
                        System.out.println("Мати");
                        Human mother = makingHuman("Woman");
                        System.out.println("Батько");
                        Human father = makingHuman("Man");
                        Family family = new Family(mother, father);
                        famList.add(family);
                        setFamilyList(famList);
                        break;
                    case 7:
                        deleteFamilyByIndex(integerInput("Ведіть індекс родини, яка має бути видалена"));
                        break;
                    case 8:
                        int id = 0;
                        String manCase = "";
                        String womanCase = "";
                        System.out.println("Редагувати сім'ю за індексом. \nУвага! Родина не може складатися більш ніж з 5 осіб! \nСhoose the option from the list below:");
                        while (true) {
                            try {
                                System.out.println(subMenu);
                                switch (integerInput("")) {
                                    case 1:
                                        id = integerInput("Народження дитини. \nВвдіть порядковий номер родини. \nНаразі є "
                                                + getAllFamilies().size() + " родин." );
                                        manCase = lineInput("Як назовемо хлопчика?");
                                        womanCase = lineInput("Як назовемо дівчинку?");
                                        bornChild(getFamilyById(id), manCase, womanCase);
                                        break;
                                    case 2:
                                        id = integerInput("Всиновлення дитини. \nВвдіть порядковий номер родини.\nНаразі є "
                                                + getAllFamilies().size() + " родин.");
                                        Human adoptedChild = new Human();
                                        while (true) {
                                            try {
                                                System.out.println(genderSubMenu);
                                                switch (integerInput("")) {
                                                    case 1:
                                                        adoptedChild = makingHuman("Man");
                                                        adoptChild(getFamilyById(id),adoptedChild );
                                                        break;
                                                    case 2:
                                                        adoptedChild = makingHuman("Woman");
                                                        adoptChild(getFamilyById(id),adoptedChild );
                                                        break;
                                                    case 3:
                                                        menu();
                                                        break;
                                                    default:
                                                        throw new InputMismatchException();

                                                }

                                            } catch (InputMismatchException e) {
                                                System.out.println(">>> >>> >>> Некоректные данные! Попробуйте ещё раз. <<< <<< <<<");
                                            }
                                        }
                                    case 3:
                                        System.out.println("Exit to the Main Menu");
                                        menu();
                                    default:
                                        throw new InputMismatchException();
                                }
                            } catch (InputMismatchException e) {
                                System.out.println(">>> >>> >>> Некоректные данные! Попробуйте ещё раз. <<< <<< <<<");
                            }
                        }
                    case 9:
                        deleteAllChildrenOlderThen(integerInput("Введіть цікавлячий вік дітей"));
                        break;
                    case 0:
                        System.out.println("exit");
                        System.exit(0);
                    default:
                        throw new InputMismatchException();
                }
            } catch (InputMismatchException e) {
                System.out.println(">>> >>> >>> Некоректные данные! Попробуйте ещё раз. <<< <<< <<<");
            }
        }

    }

}
class FamilyOverflowException extends RuntimeException{
    public FamilyOverflowException(String message) {
        super(message);
    }
}
