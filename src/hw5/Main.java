package hw5;


public class Main {
    public static void main(String[] args) {
        Human father1 = new Human("Bohdan", "Ivanenko", 1980, 82, new String[][]{{DayOfWeek.MONDAY.name(), "task1"}, {DayOfWeek.WEDNESDAY.name(), "task2"}});
        Human mother1 = new Human("Maria", "Ivanenko", 1982, 75, new String [][]{{DayOfWeek.THURSDAY.name(), "task3"}, {DayOfWeek.FRIDAY.name(), "task4"}});
        Human children1 = new Human("Taras", "Ivanenko", 2003, 82, new String [][] {{DayOfWeek.FRIDAY.name(), "task5"}, {DayOfWeek.SUNDAY.name(), "task6"}});
        Human children3 = new Human("Krystyna", "Ivanenko", 2005, 82, new String [][]{{DayOfWeek.MONDAY.name(), "task7"}, {DayOfWeek.FRIDAY.name(), "task8"}});
        Pet pet1 = new Pet(Species.DOG, "Sharick", 8, 48, new String[] {"play with ball", "eat meat"});
        System.out.println("----------HUMANS & PETS---------");
        System.out.println(father1.toString());
        System.out.println(mother1.toString());
        System.out.println(children1.toString());
        System.out.println(children3.toString());
        System.out.println(pet1);
        System.out.println("----------FAMILIES FORMATTING---------");
        System.out.println("----------FAMILY1---------");
        Family family1 = new Family( mother1,father1 );
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------CHILDREN ADDING---------");
        family1.addChild(children1);
        family1.addChild(children3);
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------CHILDREN DELETING---------");
        family1.deleteNumChild(0);
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------PET ADDING---------");
        family1.setPet(pet1);
        System.out.println(family1);
        System.out.println("----------ACTION WITH PET---------");
        family1.getFather().describePet();
        family1.getMother().greetPet();
        family1.getPet().respond();
        family1.getPet().eat();
        family1.getPet().foul();
        System.out.println("----------ACTION WITH PET (random feeding)---------");
        family1.getPet().isFed(true);
        family1.getPet().isFed(false);
        System.out.println("----------ACTION WITH GARBAGE COLLECTOR---------");
        for (int i = 0; i < 10000; i++) {
            Human hum = new Human("Human" + i, "Surname" + i, i);
        }
        Runtime.getRuntime().gc();
    }
}
