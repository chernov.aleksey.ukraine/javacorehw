package hw5;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;
    public Pet() {}
    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Species getSpecies() {return species;}
    public void setSpecies(Species species ) {this.species = species;}
    public String getNickname() {return nickname;}
    public void setNickname(String nickname) {this.nickname = nickname;}
    public int getAge() {return age;}
    public void setAge(int age) {this.age = age;}
    public int getTrickLevel() {return trickLevel;}
    public void setTrickLevel(int trickLevel) {this.trickLevel = trickLevel;}
    public String[] getHabits() {return habits;}
    public void setHabits(String[] habits) {this.habits = habits;}
    public void respond() {System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");}
    public void eat() { System.out.println("Я кушаю!");}
    public void foul() {System.out.println("Нужно хорошо замести следы...");}
    public boolean isFed (Boolean isTime) {
        Random random = new Random();
        int chance = random.nextInt(100);
        if (!isTime && chance<= this.trickLevel) {
            System.out.println("Думаю, " + this.nickname +" не голоден.");
            return false;
        } else{
            System.out.println("Хм... покормлю ка я " + this.nickname);
            return true;
        }
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && trickLevel == pet.trickLevel && Objects.equals(species, pet.species) && Objects.equals(nickname, pet.nickname) && Arrays.equals(habits, pet.habits);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }
    @Override
    public String toString() {
        String noPetString;
        if (this.getSpecies() == null){noPetString = "{null}";
        } else {
            noPetString = "{ is can fly = "+ this.getSpecies().isCanFly() + ",  number of legs = " + this.getSpecies().getNumberOfLegs() + ", has fur =" + this.getSpecies().isHasFur() +"}";
        }
        return this.getSpecies()+ noPetString + "{nickname=" + this.getNickname() + ", age= " + this.getAge() +
                ", trickLevel=" + this.getTrickLevel() + ", habits=" + Arrays.toString(this.getHabits()) + "}";
    }

    @Override
    public void finalize(){
        System.out.println("Class Pet is going to be removed");
    }
}
enum Species {
    DOG(false, 4, true), CAT(false, 4, true);
    private boolean canFly;
    private int numberOfLegs;
    private boolean hasFur;
    public boolean isCanFly() {return canFly;}
    public void setCanFly(boolean canFly) {this.canFly = canFly;}
    public int getNumberOfLegs() {return numberOfLegs;}
    public void setNumberOfLegs(int numberOfLegs) {this.numberOfLegs = numberOfLegs;}
    public boolean isHasFur() {return hasFur;}
    public void setHasFur(boolean hasFur) {this.hasFur = hasFur;}

    Species (boolean canFly, int numberOfLegs, boolean hasFur){
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

}