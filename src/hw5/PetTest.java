package hw5;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PetTest {
    private Pet module;

    @BeforeEach
    public void setUp() {
        module = new Pet();
    }

    @Test
    public void testToString() {
        String actual = module.toString();
        String expected = "null{null}{nickname=null, age= 0, trickLevel=0, habits=null}";
        Assertions.assertEquals(expected, actual);
    }
}
