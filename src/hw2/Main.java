package hw2;

import java.util.Random;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        boolean placement = random.nextBoolean();
        int col = random.nextInt(5) + 1;
        int li = random.nextInt(5) + 1;
        int[][] vector = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
        if (placement) {
            if (li == 1) li = 2;
            if (li == 5) li = 4;
            for (int i=0; i<3; i++){
                vector [i][0] = col;
                vector [i][1] = li -1 +i;
            }
        } else {
            if (col == 1) col = 2;
            if (col == 5) col = 4;
            for (int i=0; i<3; i++){
                vector [i][0] = col-1 +i;
                vector [i][1] = li;
            }
        }
        System.out.println("All set. Get ready to rumble!");
        String[][] arr = {
                {"0", "1", "2", "3", "4", "5"},
                {"1", "-", "-", "-", "-", "-"},
                {"2", "-", "-", "-", "-", "-"},
                {"3", "-", "-", "-", "-", "-"},
                {"4", "-", "-", "-", "-", "-"},
                {"5", "-", "-", "-", "-", "-"}
        };
        int line;
        int column;
        int checker = 0;
        for (; ; ) {
            for (int i = 0; i <= 5; i++) {
                System.out.println();
                for (int j = 0; j <= 5; j++) {
                    System.out.print(arr[i][j] + " | ");
                }
            }
            if (checker ==3) break;
            System.out.println();
            System.out.println("Enter line ");
            if (scanner.hasNextInt()) {
                line = scanner.nextInt();
            } else {
                scanner.next();
                continue;
            }
            System.out.println("Enter column ");
            if (scanner.hasNextInt()) {
                column = scanner.nextInt();
            } else {
                scanner.next();
                continue;
            }
            if (line > 5 || line < 1 || column > 5 || column < 1) {
                System.out.print("Wrong numbers, try again");
                continue;
            }
            for (int i = 0; i<3; i++){
                if (vector[i][0] == column && vector[i][1] == line ) {
                    if (vector[i][2] ==  0){
                        checker += 1;
                        arr[line][column] = "X";
                        vector[i][2] =  1;
                        break;
                    } else {
                        arr[line][column] = "X";
                        break;
                    }
                } else {
                    arr[line][column] = "*";
                }
            }
        }
        System.out.println();
        System.out.println("YOU HAVE WON!");
        System.out.println("TARGET LINE(S)  ARE:  " + vector[0][1] + ", " + vector[1][1] + ", " + vector[2][1]  );
        System.out.println("TARGET COLUMN(S) ARE: " + vector[0][0] + ", " + vector[1][0] + ", " + vector[2][0] );
    }
}