package hw8;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao{
    private ArrayList<Family> familyList;

    public CollectionFamilyDao(ArrayList<Family> familyList) {
        this.familyList = familyList;
    }

    @Override
    public ArrayList<Family> getAllFamilies() {
        return this.familyList;
    }

    @Override
    public Family getFamilyByIndex(int famIndex) {
        return this.familyList.get(famIndex);
    };

    @Override
    public boolean deleteFamily(int famIndex) {
        boolean isDeleted;
        if (famIndex >= this.familyList.size()) {
            return false;
        }
        this.familyList.remove(famIndex);
        return true;
    };

    @Override
    public boolean deleteFamily(Family deletedFamily) {
        return this.familyList.remove(deletedFamily);
    };

    @Override
    public void saveFamily(Family updatedFamily) {
        this.familyList.remove(updatedFamily);
        this.familyList.add(updatedFamily);
    };
}
