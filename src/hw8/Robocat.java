package hw8;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

public class Robocat extends Pet implements Foul{
    public Robocat() {
        this.setSpecies(Species.ROBOCAT);
    }

        public Robocat(String nickname, int age, int trickLevel, HashSet<String> habits ) {
            super(nickname, age, habits);
            this.setSpecies(Species.ROBOCAT);
            this.setTrickLevel(trickLevel);
        }


        @Override
        public void respond() {
            System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
        }

}
