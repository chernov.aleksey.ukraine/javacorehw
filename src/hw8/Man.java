package hw8;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

final public class Man extends Human{
    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, HashMap<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
    @Override
    public void greetPet(Pet pet) {
        for (Pet el : this.getFamily().getPet()) {
            if (el.equals(pet)) {
                System.out.println("Привет, " + el.getNickname() + "! Это я - твой хозяин!");
            }
        }
    }
@Override
    public void unicActivity() {
        System.out.println("Я чиню машину!");
    }
    @Override
    public String toString() {

        return "Man{name= '" + this.getName() + "', surname= '" + this.getSurname() + "', year= " + this.getYear() +
                ", iq= " + this.getIq() + ", schedule= " + getSchedule().toString() + "}";
    }

}