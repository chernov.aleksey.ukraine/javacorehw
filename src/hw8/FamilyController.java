package hw8;

import java.util.ArrayList;
import java.util.HashSet;

public class FamilyController {
    FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public ArrayList<Family> getAllFamilies() {
        return this.familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        this.familyService.displayAllFamilies();
    }

    public ArrayList<Family> getFamiliesBiggerThan(int number) {
        return this.familyService.getFamiliesBiggerThan(number);
    }

    public ArrayList<Family> getFamiliesLessThan(int number) {
        return this.familyService.getFamiliesLessThan(number);
    }

    public int countFamiliesWithMemberNumber(int number) {
        return this.familyService.countFamiliesWithMemberNumber(number);
    }

    public void createNewFamily(Human father, Human mother) {
        this.familyService.createNewFamily(father, mother);
    }

    public void deleteFamilyByIndex(int index) {
        this.familyService.deleteFamilyByIndex(index);
    }

    public void bornChild(Family currentFamily, String manName, String womanName) {
        this.familyService.bornChild(currentFamily, manName, womanName);
    }

    public Family adoptChild(Family currentFamily, Human adoptedChild) {
        return this.familyService.adoptChild(currentFamily, adoptedChild);
    }

    public void deleteAllChildrenOlderThen(int age) {
        this.familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return this.familyService.count();
    }

    public Family getFamilyById(int index) {
        return this.familyService.getFamilyById(index);
    }

    public HashSet<Pet> getPets(int index) {
        return this.familyService.getPets(index);
    }

    public void addPet(int familyIndex, Pet pet) {
        this.familyService.addPet(familyIndex, pet);
    }
}

