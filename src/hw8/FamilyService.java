package hw8;

import java.util.*;

public class FamilyService {
    CollectionFamilyDao collectionFamilyDao;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    public ArrayList<Family> getAllFamilies() {
        return this.collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        System.out.println("Family List:");
        for (int i = 0; i < this.getAllFamilies().size(); i++) {
            System.out.println("  №" + (i + 1) + ": " + this.getAllFamilies().get(i));
        }
    }

    public ArrayList<Family> getFamiliesBiggerThan(int number) {
        ArrayList<Family> newFamList = new ArrayList<>();
        for (int i = 0; i < this.getAllFamilies().size(); i++) {
            if (this.getAllFamilies().get(i).countFamily() > number) {
                newFamList.add(this.getAllFamilies().get(i));
            }
        }
        System.out.println(newFamList);
        return newFamList;
    }

    public ArrayList<Family> getFamiliesLessThan(int number) {
        ArrayList<Family> newFamList = new ArrayList<>();
        for (int i = 0; i < this.getAllFamilies().size(); i++) {
            if (this.getAllFamilies().get(i).countFamily() < number) {
                newFamList.add(this.getAllFamilies().get(i));
            }
        }
        System.out.println(newFamList);
        return newFamList;
    }

    public int countFamiliesWithMemberNumber(int number) {
        int counter = 0;
        for (int i = 0; i < this.getAllFamilies().size(); i++) {
            if (this.getAllFamilies().get(i).countFamily() == number) {
                counter++;
            }
        }
        return counter;
    }

    public void createNewFamily(Human father, Human mother) {
        Family newFamily = new Family(father, mother);
        this.collectionFamilyDao.saveFamily(newFamily);
    }

    public void deleteFamilyByIndex(int index) {
        this.collectionFamilyDao.deleteFamily(index);
    }

    public void bornChild(Family currentFamily, String manName, String womanName) {
        Random random = new Random();
        if (random.nextBoolean()) {
            Woman child = new Woman(womanName, currentFamily.getFather().getSurname(), 2023);
            currentFamily.addChild(child);
        } else {
            Man child = new Man(manName, currentFamily.getFather().getSurname(), 2023);
            currentFamily.addChild(child);
        }

    }

    public Family adoptChild(Family currentFamily, Human adoptedChild) {
        currentFamily.addChild(adoptedChild);
        return currentFamily;
    }

    public void deleteAllChildrenOlderThen(int age) {
        for (int i = 0; i < this.getAllFamilies().size(); i++) {
            Family currentFamily = this.getAllFamilies().get(i);
            for (int j = 0; j < currentFamily.countFamily() - 2; j++) {
                if (2023 - currentFamily.getChildren().get(j).getYear() > age) {
                    currentFamily.deleteChild(currentFamily.getChildren().get(j));
                }
            }
        }
    }

    public int count() {
        return this.collectionFamilyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return this.collectionFamilyDao.getFamilyByIndex(index);
    }

    public HashSet<Pet> getPets(int index) {
        if(this.getFamilyById(index).getPet() == null) this.getFamilyById(index).setPet(new HashSet<Pet>());
        return this.getFamilyById(index).getPet();
    }

    public void addPet(int familyIndex, Pet pet) {
     if(this.getFamilyById(familyIndex).getPet() == null) this.getFamilyById(familyIndex).setPet(new HashSet<Pet>(Set.of(pet)));
     this.getFamilyById(familyIndex).getPet().add(pet);
    }
}

