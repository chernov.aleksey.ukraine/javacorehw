package hw4;

import java.util.Arrays;
import java.util.Objects;
public class Family {
    static{
        System.out.println("Class Family loading...");
    }
    {
        System.out.println("Class Family instance created");
    }
    private Human mother;
    private Human father;
    private Human[] children = {};
    private Pet pet;
    public Human getMother() {return mother;}
    public void setMother(Human mother) {
        this.mother = mother;
        this.mother.setFamily(this);}
    public Human getFather() {return father;}
    public void setFather(Human father) {
        this.father = father;
        this.father.setFamily(this);}
    public Human[] getChildren() {return children;}
    public void setChildren(Human[] children) {this.children = children;}
    public Pet getPet() {return pet; }
    public void setPet(Pet pet) {this.pet = pet;}
    private Family (){}
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.father.setFamily(this);
        this.mother.setFamily(this);
    }
    public Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.father.setFamily(this);
        this.mother.setFamily(this);
    }
    public Family(Human mother, Human father, Human[] children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.father.setFamily(this);
        this.mother.setFamily(this);
    }
    public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
        this.father.setFamily(this);
        this.mother.setFamily(this);
    }
    public void addChild(Human newChild) {
        this.children = Arrays.copyOf(this.children, this.children.length + 1);
        this.children[this.children.length - 1] = newChild;
        newChild.setFamily(this);
    }
    public void deleteChild(Human deletedChild) {
        int j=0;
        Human[] updateChildren = new Human[this.getChildren().length - 1];
        for (int i = 0; i < this.getChildren().length; i++) {
            if (this.getChildren()[i].equals(deletedChild)) {
                this.getChildren()[i].setFamily(new Family());
                } else {
                updateChildren[j] = this.getChildren()[i];
                j++;
            }
        }
        this.setChildren(updateChildren);
    }
    public void deleteNumChild(int number) {
        int j=0;
        Human[] updateChildren = new Human[this.getChildren().length - 1];
        for (int i = 0; i < this.getChildren().length; i++) {
            if (i == number) {
                this.getChildren()[i].setFamily(new Family());
            } else {
                updateChildren[j] = this.getChildren()[i];
                j++;
            }
        }
        this.setChildren(updateChildren);
    }
    public int countFamily() {
        return 2 + this.getChildren().length;
    }
    public String childrenToString() {
        String children = "{";
        for (int i = 0; i < this.getChildren().length; i++) {
            children += this.getChildren()[i].toString() + ", ";
        }
        children += "}";
        return children;
    }
    @Override
    public String toString() {
        String familyPet = ( this.pet == null) ? "No pets" : pet.toString();
        return "FAMILY:\nmother: " + mother.toString() +
                "\nfather: " + father.toString() +
                "\nchildren: " + this.childrenToString() +
                "\npet: " + familyPet;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) && father.equals(family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
}
