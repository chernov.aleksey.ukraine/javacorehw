package hw9;;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.time.temporal.ChronoUnit.YEARS;

public class Human implements HumanCreator {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private HashMap<DayOfWeek, String> schedule = new HashMap<>();;
    private Family family;

    public Human() {  }
    public Human(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        long birthDateMilli = LocalDate.parse(birthDate, formatter).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        this.birthDate = birthDateMilli;
    }
    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        long birthDateMilli = LocalDate.parse(birthDate, formatter).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        this.birthDate = birthDateMilli;
        this.iq = iq;
        }

    public Human(String name, String surname, String birthDate, int iq, HashMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        long birthDateMilli = LocalDate.parse(birthDate, formatter).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        this.birthDate = birthDateMilli;
        this.iq = iq;
        this.schedule = schedule;
    }
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getSurname() {return surname;}
    public void setSurname(String surname) {this.surname = surname;}
    public long getBirthDate() {return birthDate;}
    public void setBirthDate(String birthDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        long birthDateMilli = LocalDate.parse(birthDate, formatter).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        this.birthDate = birthDateMilli;}
    public int getIq() {return iq;}
    public void setIq(int iq) {this.iq = iq;}
    public HashMap<DayOfWeek, String> getSchedule() {return schedule;}
    public void setSchedule(HashMap<DayOfWeek, String> schedule) {this.schedule = schedule;}
    public Family getFamily() {return family;}
    public void setFamily(Family family) {this.family = family;}
    public void greetPet(Pet pet){};
    public void unicActivity(){};
    public void describePet(Pet pet) {
        for (Pet el : this.getFamily().getPet()) {
            if (el.equals(pet)) {
                String petTrickLevel = el.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
                System.out.println("У меня есть " + el.getSpecies() + ", ему " + el.getAge() + " лет, он " + petTrickLevel + ".");
            }
        }
    }
    public String ageStringer() {
        LocalDate date = Instant.ofEpochMilli(this.birthDate).atZone(ZoneId.systemDefault()).toLocalDate();;
        return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }
    public int[] ageCounter (){
        int[] returnedArray = {0, 0 , 0};
        Date date = new Date();
        long today = date.getTime();
        int days = (int)((today - this.getBirthDate())/86400000);
        int years = (int)(days/365.25);
        int month = (int)((days - (int)(years*365.25))/30.45833333);
        int daysWOYearsMonth = days - (int)(years*365.25) - (int)(month*30.45833333);
        returnedArray[0] = years;
        returnedArray[1] = month;
        returnedArray[2] = daysWOYearsMonth;
        return returnedArray;
    }
    public String describeAge(){
        return this.getName() + " was born " + this.ageStringer() + " and has been living for " + this.ageCounter()[0] + " years, " +  this.ageCounter()[1] +
     " month and " + this.ageCounter()[2] + " days!";}
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate && iq == human.iq && name.equals(human.name) && surname.equals(human.surname) && family.equals(human.family);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, family);
    }

    @Override
    public void bornChild() {
        String manNames[] = {"Oleg", "Igor", "Sergiy"};
        String womanNames[] = {"Olena", "Ivanka", "Ganna"};
        Random random = new Random();
        if (random.nextBoolean()) {
            getFamily().addChild(new Man(manNames[random.nextInt(3)], getFamily().getFather().getSurname(), "01/01/2023", (getFamily().getFather().getIq()+getFamily().getMother().getIq())/2, new HashMap<>() ));
        } else{
            getFamily().addChild(new Woman(womanNames[random.nextInt(3)], getFamily().getFather().getSurname(), "01/01/2023", (getFamily().getFather().getIq()+getFamily().getMother().getIq())/2, new HashMap<>()));
        }
    }
}
enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY,
}