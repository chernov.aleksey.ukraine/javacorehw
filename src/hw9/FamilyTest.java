package hw9;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Objects;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;

public class FamilyTest {
    private Family module;

    @BeforeEach
    public void setUp() {
        module = new Family();
    }

    @Test
    public void testToString() {
        String actual = module.toString();
        String expected = "FAMILY: null";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testDeleteExistingChild() {
        Human child = new Human();
        Human mother = new Human();
        Human father = new Human();
        Family module = new Family(mother, father, new ArrayList<Human>(Set.of(child)));
        module.deleteChild(child);
        int actual = module.getChildren().size();
        int expected = 0;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testDeleteNonExistingChild() {
        Human existingChild = new Human("name1", "lastname1", "01/01/2000");
        Human nonExistingChild = new Human("name2", "lastname2", "01/01/2003");
        Human mother = new Human();
        Human father = new Human();
        Family module = new Family(mother,father, new ArrayList<Human>(Set.of(existingChild)));
        module.deleteChild(nonExistingChild);
        int actual = module.getChildren().size();
        int expected = 1;
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void testDeleteExistingNumChild() {
        Human child = new Human();
        Human mother = new Human();
        Human father = new Human();
        Family module = new Family(mother, father, new ArrayList<Human>(Set.of(child)));
        module.deleteNumChild(0);
        int actual = module.getChildren().size();
        int expected = 0;
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void testDeleteNonExistingNumChild() {
        Human child = new Human();
        Human mother = new Human();
        Human father = new Human();
        Family module = new Family(mother, father, new ArrayList<Human>(Set.of(child)));
        module.deleteNumChild(1);
        int actual = module.getChildren().size();
        int expected = 1;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testAddChild() {
        Human child = new Human();
        Human mother = new Human();
        Human father = new Human();
        Family module = new Family(mother, father);
        module.addChild(child);
        int actual = module.getChildren().size();
        int expected = 1;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testCountFamily() {
        Human child = new Human();
        Human mother = new Human();
        Human father = new Human();
        Family module = new Family(mother, father, new ArrayList<Human>(Set.of(child)));
        int actual = module.countFamily();
        int expected = 3;
        Assertions.assertEquals(expected, actual);
    }


}
