package hw9;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FamilyServiceTest {
    @Mock
    private CollectionFamilyDao collectionFamilyDao;
    @InjectMocks
    private FamilyService familyService;
    Man father1 = new Man("Bohdan", "Ivanenko", "01/01/1980", 82, new HashMap<DayOfWeek, String>());
    Man father2 = new Man("Semen", "Petrenko", "01/01/1978", 89, new HashMap<DayOfWeek, String>());
    Woman mother1 = new Woman("Maria", "Ivanenko", "01/01/1982", 75, new HashMap<DayOfWeek, String>());
    Woman mother2 = new Woman("Kateryna", "Petrenko", "01/01/1980", 91, new HashMap<DayOfWeek, String>());
    Man children1 = new Man("Taras", "Ivanenko", "01/01/2003", 82, new HashMap<DayOfWeek, String>());
    Man children2 = new Man("Stepan", "Petrenko", "01/01/2001", 89, new HashMap<DayOfWeek, String>());
    Dog pet1 = new Dog("Sharick", 8, 48, new HashSet<>(Set.of("play with ball", "eat meat")));
    Fish pet2 = new Fish("Goldy", 2, 7, new HashSet<>(Set.of("say 'Booolb'", "looking TV-set in front of")));
    @Test
    public void testGetFamiliesBiggerThan() {
        when(collectionFamilyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(new Family(mother1, father1, new ArrayList<Human>(Set.of(children1)), new HashSet<Pet>(Set.of(pet1))))));
        List<Family> actualFamilies = familyService.getFamiliesBiggerThan(2);
        assertEquals(1, actualFamilies.size());}
    @Test
    public void testGetFamiliesLessThan() {
        when(collectionFamilyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(new Family(mother1, father1, new ArrayList<Human>(Set.of(children1)), new HashSet<Pet>(Set.of(pet1))))));
        List<Family> actualFamilies = familyService.getFamiliesLessThan(4);
        assertEquals(1, actualFamilies.size());}

    @Test
    public void testCountFamiliesWithMemberNumber() {
        when(collectionFamilyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(new Family(mother1, father1, new ArrayList<Human>(Set.of(children1)), new HashSet<Pet>(Set.of(pet1))))));
        int actual = familyService.countFamiliesWithMemberNumber(3);
        assertEquals(1, actual);}
    @Test
    public void testCreateNewFamily() {
        CollectionFamilyDao collectionFamilyDao1 = new CollectionFamilyDao(new ArrayList<>(List.of(new Family(mother1, father1))));
        FamilyService familyService1 = new FamilyService(collectionFamilyDao1);
        Family family2 = new Family(father2, mother2);
        collectionFamilyDao1.saveFamily(family2);
        int actual = familyService1.count();
        assertEquals(2, actual);}
    @Test
    public void testDeleteFamilyByIndex() {
        CollectionFamilyDao collectionFamilyDao1 = new CollectionFamilyDao(new ArrayList<>(List.of(new Family(mother1, father1), new Family(father2, mother2))));
        FamilyService familyService1 = new FamilyService(collectionFamilyDao1);
        collectionFamilyDao1.deleteFamily(1);
        int actual = familyService1.count();
        assertEquals(1, actual);}
    @Test
    public void testBornChild() {
        Family family1 = new Family(mother1, father1);
        Family family2 = new Family(mother2, father2);
        CollectionFamilyDao collectionFamilyDao1 = new CollectionFamilyDao(new ArrayList<>(List.of(family1, family2)));
        FamilyService familyService1 = new FamilyService(collectionFamilyDao1);
        familyService1.bornChild(family2, "Name1", "Name2");
        int actual = family2.getChildren().size();
        assertEquals(1, actual);}
    @Test
    public void testDeleteAllChildrenOlderThen() {
        Family family1 = new Family(mother1, father1, new ArrayList<Human>(Set.of(children1, children2)), new HashSet<Pet>(Set.of(pet1)));
        Family family2 = new Family(mother2, father2);
        CollectionFamilyDao collectionFamilyDao1 = new CollectionFamilyDao(new ArrayList<>(List.of(family1, family2)));
        FamilyService familyService1 = new FamilyService(collectionFamilyDao1);
        familyService1.deleteAllChildrenOlderThen(21);
        int actual = family1.getChildren().size();
        assertEquals(1, actual);}
    @Test
    public void testCount() {
        Family family1 = new Family(mother1, father1, new ArrayList<Human>(Set.of(children1, children2)), new HashSet<Pet>(Set.of(pet1)));
        Family family2 = new Family(mother2, father2);
        CollectionFamilyDao collectionFamilyDao1 = new CollectionFamilyDao(new ArrayList<>(List.of(family1, family2)));
        FamilyService familyService1 = new FamilyService(collectionFamilyDao1);
        int actual = familyService1.count();
        assertEquals(2, actual);}
    @Test
    public void testGetFamilyById() {
        Family family1 = new Family(mother1, father1, new ArrayList<Human>(Set.of(children1, children2)), new HashSet<Pet>(Set.of(pet1)));
        Family family2 = new Family(mother2, father2);
        CollectionFamilyDao collectionFamilyDao1 = new CollectionFamilyDao(new ArrayList<>(List.of(family1, family2)));
        FamilyService familyService1 = new FamilyService(collectionFamilyDao1);
        Family expected = familyService1.getAllFamilies().get(0);
        Family actual = familyService1.getFamilyById(0);
        assertEquals(expected, actual);    }
    @Test
    public void testGetFamilyById1(){
        when(collectionFamilyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(new Family( mother1,father1,new ArrayList<Human>(Set.of(children1)), new HashSet<Pet>(Set.of(pet1))))));
        Family expected = this.collectionFamilyDao.getAllFamilies().get(0);
        Family actual = this.familyService.getAllFamilies().get(0);
       assertEquals(expected, actual);}
    @Test
    public void testGetPets() {
        when(collectionFamilyDao.getFamilyByIndex(0)).thenReturn(new Family( mother1,father1,new ArrayList<Human>(Set.of(children1)), new HashSet<Pet>(Set.of(pet1))));
        int actual = this.familyService.getPets(0).size();
        Assertions.assertEquals(1, actual);}
    @Test
    public void testAddPet() {
        Family family1 = new Family(mother1, father1, new ArrayList<Human>(Set.of(children1, children2)), new HashSet<Pet>(Set.of(pet1)));
        Family family2 = new Family(mother2, father2);
        CollectionFamilyDao collectionFamilyDao1 = new CollectionFamilyDao(new ArrayList<>(List.of(family1, family2)));
        FamilyService familyService1 = new FamilyService(collectionFamilyDao1);
        int expected = familyService1.getPets(1).size() + 1;
        familyService1.addPet(1, pet2);
        int actual = familyService1.getPets(1).size();
        Assertions.assertEquals(expected, actual);}
}
