package hw9;

import java.util.HashMap;
import java.util.Random;

final public class Woman extends Human{
    public Woman() {
    }

    public Woman(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }
    public Woman(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }
    public Woman(String name, String surname, String birthDate, int iq, HashMap<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }
    @Override
    public void greetPet(Pet pet ) {
        for (Pet el : this.getFamily().getPet()) {
            if ( el.equals(pet)) {
                System.out.println("Привет, " + el.getNickname() + "! Это я - твоя хозяйка!");
            }
        }


    }
    @Override
    public void unicActivity() {
        System.out.println("Я крашусь");
    }
    @Override
    public String toString() {
        return "Woman{name= '" + this.getName() + "', surname= '" + this.getSurname() + "', birthDate= " + this.ageStringer() +
                ", iq= " + this.getIq() + ", schedule= " + getSchedule().toString() + "}";
    }

}
