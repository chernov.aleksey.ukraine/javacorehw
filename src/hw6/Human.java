package hw6;
import java.util.Objects;
import java.util.Random;
public class Human implements HumanCreator {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule = {};
    private Family family;

    public Human() {  }
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getSurname() {return surname;}
    public void setSurname(String surname) {this.surname = surname;}
    public int getYear() {return year;}
    public void setYear(int year) {this.year = year;}
    public int getIq() {return iq;}
    public void setIq(int iq) {this.iq = iq;}
    public String[][] getSchedule() {return schedule;}
    public void setSchedule(String[][] schedule) {this.schedule = schedule;}
    public Family getFamily() {return family;}
    public void setFamily(Family family) {this.family = family;}
    public void greetPet(){};
    public void unicActivity(){};
    public void describePet() {
        Pet pet = family.getPet();
        String petTrickLevel = pet.getTrickLevel() > 50? "очень хитрый" : "почти не хитрый";
        System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " лет, он " + petTrickLevel + ".");
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && iq == human.iq && name.equals(human.name) && surname.equals(human.surname) && family.equals(human.family);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, family);
    }

    @Override
    public void bornChild() {
        String manNames[] = {"Oleg", "Igor", "Sergiy"};
        String womanNames[] = {"Olena", "Ivanka", "Ganna"};
        Random random = new Random();
        if (random.nextBoolean()) {
            getFamily().addChild(new Man(manNames[random.nextInt(3)], getFamily().getFather().getSurname(), 2023, (getFamily().getFather().getIq()+getFamily().getMother().getIq())/2, new String[][]{} ));
        } else{
            getFamily().addChild(new Woman(womanNames[random.nextInt(3)], getFamily().getFather().getSurname(), 2023, (getFamily().getFather().getIq()+getFamily().getMother().getIq())/2, new String[][]{}));
        }
    }
}
enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY,
}