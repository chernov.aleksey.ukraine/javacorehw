package hw6;

import java.util.Arrays;
import java.util.Random;

public class Cat extends Pet implements Foul{
    public Cat(String nickname, int age, int trickLevel,String[] habits ) {
        super(nickname, age, habits);
        this.setSpecies(Species.CAT);
        this.setTrickLevel(trickLevel);
    }


    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }
    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }


}

