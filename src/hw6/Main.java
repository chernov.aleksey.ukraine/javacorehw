package hw6;


import hw4.Pet;

public class Main {
    public static void main(String[] args) {
        Man father1 = new Man("Bohdan", "Ivanenko", 1980, 82, new String[][]{{DayOfWeek.MONDAY.name(), "task1"}, {DayOfWeek.WEDNESDAY.name(), "task2"}});
        Woman mother1 = new Woman("Maria", "Ivanenko", 1982, 75, new String [][]{{DayOfWeek.THURSDAY.name(), "task3"}, {DayOfWeek.FRIDAY.name(), "task4"}});
        Man children1 = new Man("Taras", "Ivanenko", 2003, 82, new String [][] {{DayOfWeek.FRIDAY.name(), "task5"}, {DayOfWeek.SUNDAY.name(), "task6"}});
        Woman children3 = new Woman("Krystyna", "Ivanenko", 2005, 82, new String [][]{{DayOfWeek.MONDAY.name(), "task7"}, {DayOfWeek.FRIDAY.name(), "task8"}});

        Dog pet1 = new Dog("Sharick", 8, 48, new String[] {"play with ball", "eat meat"});
        Fish pet2 = new Fish( "Goldy", 2, 7, new String[] {"say 'Booolb'", "looking TV-set in front of" });
        Cat pet3 = new Cat( "Barsick", 6, 56, new String[] {"say 'Meow'", "eat fish"});
        System.out.println("----------HUMANS & PETS---------");
        System.out.println(father1);
        System.out.println(mother1);
        System.out.println(children1);
        System.out.println(children3);
        System.out.println(pet1);
        System.out.println(pet2);
        System.out.println(pet3);
        System.out.println("----------FAMILIES FORMATTING---------");
        System.out.println("----------FAMILY1---------");
        Family family1 = new Family( mother1,father1 );
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------FAMILY1 MEMBERS ACTIVITIES---------");
        family1.getMother().unicActivity();
        family1.getFather().unicActivity();
        System.out.println("----------CHILDREN ADDING---------");
        family1.addChild(children1);
        family1.addChild(children3);
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------BABY BORN ADDING---------");
        family1.getMother().bornChild();
        family1.getFather().bornChild();
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------CHILDREN DELETING---------");
        family1.deleteNumChild(3);
        family1.deleteNumChild(1);
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------PET ADDING---------");
        family1.setPet(pet3);
        System.out.println(family1);
        System.out.println("----------ACTION WITH PET---------");
        family1.getFather().describePet();
        family1.getMother().greetPet();
        family1.getFather().greetPet();
        family1.getPet().respond();
        family1.getPet().eat();
        family1.getPet().foul();
        System.out.println("----------ACTION WITH PET (random feeding)---------");
        family1.getPet().isFed(true);
        family1.getPet().isFed(false);
        Runtime.getRuntime().gc();
    }
}
