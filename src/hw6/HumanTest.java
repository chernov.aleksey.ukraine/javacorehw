package hw6;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HumanTest {
    private Human module;

    @BeforeEach
    public void setUp() {
        module = new Man();
    }

    @Test
    public void testToString() {
        String actual = module.toString();
        String expected = "Man{name= 'null', surname= 'null', year= 0, iq= 0, schedule= []}";
        Assertions.assertEquals(expected, actual);
    }
}
