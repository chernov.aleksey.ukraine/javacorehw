package hw6;

import java.util.Arrays;
import java.util.Random;

public class Dog extends Pet {
    public Dog() {
    }
    public Dog(String nickname, int age,int trickLevel , String[] habits ) {
        super(nickname, age, habits);
        this.setSpecies(Species.DOG);
        this.setTrickLevel(trickLevel);
    }


    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }
    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
}
