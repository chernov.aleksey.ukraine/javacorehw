package hw6;

import java.util.Random;

final public class Woman extends Human{
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }
    @Override
    public void greetPet() {
        System.out.println("Привет, " + this.getFamily().getPet().getNickname() + "! Это я - твоя хозяйка!");
    }
    @Override
    public void unicActivity() {
        System.out.println("Я крашусь");
    }
    @Override
    public String toString() {
        String schedule = "[";
        for (int i = 0; i < this.getSchedule().length; i++) {
            schedule += "[";
            for (int j = 0; j < this.getSchedule()[i].length; j++) {
                schedule += this.getSchedule()[i][j];
                if (!(this.getSchedule()[i].length == j + 1)) schedule += ", ";
            }
            schedule += "]";
            if (!(this.getSchedule().length == i + 1)) schedule += ", ";
        }
        schedule += "]";
        return "Woman{name= '" + this.getName() + "', surname= '" + this.getSurname() + "', year= " + this.getYear() +
                ", iq= " + this.getIq() + ", schedule= " + schedule + "}";
    }

}
