package hw7;

import java.util.HashMap;
import java.util.Map;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        HashMap <DayOfWeek, String> schedule1 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule2 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule3 = new HashMap<DayOfWeek, String>();
        schedule1.put(DayOfWeek.MONDAY, "task1");
        schedule1.put(DayOfWeek.THURSDAY, "task2");
        schedule2.put(DayOfWeek.THURSDAY, "task3");
        schedule2.put(DayOfWeek.WEDNESDAY, "task4");
        schedule3.put(DayOfWeek.SATURDAY, "task5");
        schedule3.put(DayOfWeek.SUNDAY, "task6");
        Man father1 = new Man("Bohdan", "Ivanenko", 1980, 82, schedule1);
        Woman mother1 = new Woman("Maria", "Ivanenko", 1982, 75, schedule2);
        Man children1 = new Man("Taras", "Ivanenko", 2003, 82, schedule3);
        Dog pet1 = new Dog("Sharick", 8, 48, new HashSet<>  (Set.of("play with ball", "eat meat")) );
        Fish pet2 = new Fish( "Goldy", 2, 7, new HashSet<>  (Set.of("say 'Booolb'", "looking TV-set in front of" )));
        Cat pet3 = new Cat( "Barsick", 6, 56, new HashSet<>  (Set.of("say 'Meow'", "eat fish")));
        System.out.println("----------HUMANS & PETS---------");
        System.out.println(father1);
        System.out.println(father1);
        System.out.println(mother1);
        System.out.println(children1);
        System.out.println(pet1);
        System.out.println(pet2);
        System.out.println(pet3);
        System.out.println("----------FAMILIES FORMATTING---------");
        System.out.println("----------FAMILY1---------");
        Family family1 = new Family( mother1,father1 );
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------FAMILY1 MEMBERS ACTIVITIES---------");
        family1.getMother().unicActivity();
        family1.getFather().unicActivity();
        System.out.println("----------CHILDREN ADDING---------");
        family1.addChild(children1);
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------BABY BORN ADDING---------");
        family1.getMother().bornChild();
        family1.getFather().bornChild();
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------CHILDREN DELETING---------");
        family1.deleteNumChild(2);
        family1.deleteChild(children1);
        System.out.println(family1);
        System.out.println(family1.countFamily());
        System.out.println("----------PET ADDING---------");
        family1.setPet(new HashSet<>(Set.of(pet1, pet2, pet3)));
        System.out.println(family1);
        System.out.println("----------HUMAN ACTIONS WITH PETS---------");
        family1.getFather().describePet(pet1);
        family1.getMother().greetPet(pet2);
        family1.getFather().greetPet(pet3);
        System.out.println("----------PETS RESPONCES---------");
        for ( Pet el: family1.getPet()) {
            System.out.println(el.getSpecies()+" "+el.getNickname());
            el.respond();
            el.eat();
            el.foul();
            el.isFed(false);
    }
        Runtime.getRuntime().gc();
    }
}
