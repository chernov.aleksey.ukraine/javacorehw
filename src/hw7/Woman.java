package hw7;

import java.util.HashMap;
import java.util.Random;

final public class Woman extends Human{
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, HashMap<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }
    @Override
    public void greetPet(Pet pet ) {
        for (Pet el : this.getFamily().getPet()) {
            if ( el.equals(pet)) {
                System.out.println("Привет, " + el.getNickname() + "! Это я - твоя хозяйка!");
            }
        }


    }
    @Override
    public void unicActivity() {
        System.out.println("Я крашусь");
    }
    @Override
    public String toString() {
        return "Woman{name= '" + this.getName() + "', surname= '" + this.getSurname() + "', year= " + this.getYear() +
                ", iq= " + this.getIq() + ", schedule= " + getSchedule().toString() + "}";
    }

}
