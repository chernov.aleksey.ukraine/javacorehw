package hw7;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

public class Fish extends Pet{

    public Fish() {
    }
    public Fish(String nickname, int age, int trickLevel, HashSet<String> habits ) {
        super(nickname, age, habits);
        this.setSpecies(Species.FISH);
        this.setTrickLevel(trickLevel);
    }


    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }

}
