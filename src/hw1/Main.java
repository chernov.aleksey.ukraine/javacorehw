package hw1;
import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        String [][] questions= {
                {"1939", "1945", "1917"},
                {"When did the World War II begin?", "When did the World War II finish?", "When did the october coup in russia take place?"}
        };
        int j = random.nextInt(3);
        int number = Integer.parseInt(questions[0][j]);
        int a;
        int counter = 0;
        int [] versions  ={0};
        System.out.println("Enter your name, pls");
        String name = scanner.nextLine();
        System.out.println("Let the game begin!");
        System.out.println(questions[1][j]);
        for (;;) {
            System.out.println("Enter a year");
            if (scanner.hasNextInt()) {
                a = scanner.nextInt();
            } else {
                scanner.next();
                continue;
            }
            if (counter == 0) {
                versions[0] = a;
            } else {
                versions = Arrays.copyOf(versions, versions.length + 1);
                versions[versions.length - 1] = a;
            }
            counter++;
            if (a < number) {
                System.out.println("Your year is too small. Please, try again.");
            } else if (a > number) {
                System.out.println("Your year is too big. Please, try again.");
            } else break;
        }
        System.out.println("Congratulations,  " + name + ", " + a + " is the right answer!!!");
        Arrays.sort(versions);
        System.out.println("The versions of the year which you've entered:");
        for (int version : versions){
            System.out.println(version);
        }
        scanner.close();
    }
}