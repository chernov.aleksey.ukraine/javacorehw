package hw10;

import java.util.*;
import java.util.stream.Collectors;

public class FamilyService {
    CollectionFamilyDao collectionFamilyDao;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    public ArrayList<Family> getAllFamilies() {
        return this.collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        System.out.println("Family List:");
        this.getAllFamilies()
                .stream()
                .forEach(e-> System.out.println(e));

    }

    public List<Family> getFamiliesBiggerThan(int number) {
        return this.getAllFamilies()
                .stream()
                .filter(e -> e.countFamily() > number)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int number) {
        return this.getAllFamilies()
                .stream()
                .filter(e -> e.countFamily() < number)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int number) {
        return this.getAllFamilies()
                .stream()
                .filter(e -> e.countFamily() == number)
                .collect(Collectors.toList()).size();
    }

    public void createNewFamily(Human father, Human mother) {
        Family newFamily = new Family(father, mother);
        this.collectionFamilyDao.saveFamily(newFamily);
    }

    public void deleteFamilyByIndex(int index) {
        this.collectionFamilyDao.deleteFamily(index);
    }

    public void bornChild(Family currentFamily, String manName, String womanName) {
        Random random = new Random();
        if (random.nextBoolean()) {
            Woman child = new Woman(womanName, currentFamily.getFather().getSurname(), "01/01/2023");
            currentFamily.addChild(child);
        } else {
            Man child = new Man(manName, currentFamily.getFather().getSurname(), "01/01/2023");
            currentFamily.addChild(child);
        }

    }

    public Family adoptChild(Family currentFamily, Human adoptedChild) {
        currentFamily.addChild(adoptedChild);
        return currentFamily;
    }

    public void deleteAllChildrenOlderThen(int age) {
        this.getAllFamilies()
//                .stream()
                .forEach(family -> family.getChildren().removeIf(e -> e.ageCounter()[0]>age)
//                {for (int j = 0; j < family.getChildren().size(); j++) {
//                        if (family.getChildren().get(j).ageCounter()[0] > age) {
//                            family.deleteChild(family.getChildren().get(j));}}}
                );}

    public int count() {
        return this.collectionFamilyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return this.collectionFamilyDao.getFamilyByIndex(index);
    }

    public HashSet<Pet> getPets(int index) {
        if(this.getFamilyById(index).getPet() == null) this.getFamilyById(index).setPet(new HashSet<Pet>());
        return this.getFamilyById(index).getPet();
    }

    public void addPet(int familyIndex, Pet pet) {
     if(this.getFamilyById(familyIndex).getPet() == null) this.getFamilyById(familyIndex).setPet(new HashSet<Pet>(Set.of(pet)));
     this.getFamilyById(familyIndex).getPet().add(pet);
    }
}

