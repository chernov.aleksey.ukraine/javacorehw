package hw10;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import java.util.HashSet;
import java.util.Set;
public abstract class Pet implements Foul {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private HashSet<String> habits = new HashSet<>();;

    public Pet() {}
    public Pet(String nickname, int age,  HashSet<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.habits = habits;
    }
    public Species getSpecies() {return species;}
    public void setSpecies(Species species ) {this.species = species;}
    public String getNickname() {return nickname;}
    public void setNickname(String nickname) {this.nickname = nickname;}
    public int getAge() {return age;}
    public void setAge(int age) {this.age = age;}
    public int getTrickLevel() {return trickLevel;}
    public void setTrickLevel(int trickLevel) {this.trickLevel = trickLevel;}
    public HashSet<String> getHabits() {return habits;}
    public void setHabits(HashSet<String> habits) {this.habits = habits;}
    public abstract void respond();
    public void eat() {
        System.out.println("Я кушаю!");}

    public boolean isFed(Boolean isTime) {
        Random random = new Random();
        int chance = random.nextInt(100);
        if (!isTime && chance <= this.getTrickLevel()) {
            System.out.println("Думаю, " + this.getNickname() + " не голоден.");
            return false;
        } else {
            System.out.println("Хм... покормлю ка я " + this.getNickname());
            return true;
        }
    }
    @Override
    public String toString() {
        String noPetString;
        if (this.getSpecies() == null){noPetString = "{null}";
        } else {
            noPetString = "{ is can fly = "+ this.getSpecies().isCanFly() + ",  number of legs = " + this.getSpecies().getNumberOfLegs() + ", has fur =" + this.getSpecies().isHasFur() +"}";
        }
        return this.getSpecies()+ noPetString + "{nickname=" + this.getNickname() + ", age= " + this.getAge() +
                ", trickLevel=" + this.getTrickLevel() + ", habits=" + this.getHabits().toString() + "}";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && trickLevel == pet.trickLevel && Objects.equals(species, pet.species) && Objects.equals(nickname, pet.nickname) && Objects.equals(habits, pet.habits);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + this.habits.hashCode() ;
        return result;
    }

enum Species {
    DOG(false, 4, true), CAT(false, 4, true), UNKNOWN(), FISH(false, 0, false), ROBOCAT(false, 4, false);
    private boolean canFly;
    private int numberOfLegs;
    private boolean hasFur;

    public boolean isCanFly() {
        return canFly;
    }

    public void setCanFly(boolean canFly) {
        this.canFly = canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public void setNumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }

    Species() {
    }

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
}
}