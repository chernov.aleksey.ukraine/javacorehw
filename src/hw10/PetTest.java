package hw10;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PetTest {
    private Dog module;

    @BeforeEach
    public void setUp() {
        module = new Dog();
    }

    @Test
    public void testToString() {
        String actual = module.toString();
        String expected = "UNKNOWN{ is can fly = false,  number of legs = 0, has fur =false}{nickname=null, age= 0, trickLevel=0, habits=[]}";
        Assertions.assertEquals(expected, actual);
    }
}
