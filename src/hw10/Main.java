package hw10;

import java.time.LocalDate;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        HashMap <DayOfWeek, String> schedule1 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule2 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule3 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule4 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule5 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule6 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule7 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule8 = new HashMap<DayOfWeek, String>();
        HashMap <DayOfWeek, String> schedule9 = new HashMap<DayOfWeek, String>();

        schedule1.put(DayOfWeek.MONDAY, "task1");
        schedule2.put(DayOfWeek.TUESDAY, "task2");
        schedule3.put(DayOfWeek.WEDNESDAY, "task3");
        schedule4.put(DayOfWeek.THURSDAY, "task4");
        schedule5.put(DayOfWeek.FRIDAY, "task5");
        schedule6.put(DayOfWeek.SATURDAY, "task6");
        schedule7.put(DayOfWeek.SUNDAY, "task7");
        schedule8.put(DayOfWeek.MONDAY, "task8");
        schedule9.put(DayOfWeek.TUESDAY, "task9");

        Man father1 = new Man("Bohdan", "Ivanenko", "01/01/1980", 82, schedule1);
        Man father2 = new Man("Semen", "Petrenko", "01/01/1978", 89, schedule2);
        Man father3 = new Man("Stepan", "Sydorenko", "01/01/1976", 85, schedule3);
        Woman mother1 = new Woman("Maria", "Ivanenko", "01/01/1982", 75, schedule4);
        Woman mother2 = new Woman("Kateryna", "Petrenko", "01/01/1980", 91, schedule5);
        Woman mother3 = new Woman("Galyna", "Sydrenko", "01/01/1978", 83, schedule6);
        Man children1 = new Man("Taras", "Ivanenko", "01/01/2003", 82, schedule7);
        Man children2 = new Man("Stepan", "Petrenko", "01/01/2007", 89, schedule8);
        Woman children3 = new Woman("Krystyna", "Sydorenko", "01/01/1999", 82, schedule9);
        Dog pet1 = new Dog("Sharick", 8, 48, new HashSet<>  (Set.of("play with ball", "eat meat")) );
        Fish pet2 = new Fish( "Goldy", 2, 7, new HashSet<>  (Set.of("say 'Booolb'", "looking TV-set in front of" )));
        Cat pet3 = new Cat( "Barsick", 6, 56, new HashSet<>  (Set.of("say 'Meow'", "eat fish")));

        System.out.println("----------HUMANS & PETS---------");
        System.out.println(father1);
        System.out.println("----------DESCRIBE AGE---------");
        System.out.println(father1.describeAge());
        System.out.println(mother1);
        System.out.println("----------DESCRIBE AGE---------");
        System.out.println(mother1.describeAge());
        System.out.println(children1);
        System.out.println(father2);
        System.out.println(mother2);
        System.out.println(children2);
        System.out.println(father3);
        System.out.println(mother3);
        System.out.println(children3);
        System.out.println(pet1);
        System.out.println(pet2);
        System.out.println(pet3);

        System.out.println("----------FAMILIES DAO FORMATION---------");
        Family family1 = new Family( mother1,father1,new ArrayList<Human>(Set.of(children1)), new HashSet<Pet>(Set.of(pet1)));
        Family family2 = new Family( mother2,father2,new ArrayList<Human>(Set.of(children2)), new HashSet<Pet>(Set.of(pet2)));
        Family family3 = new Family( mother3,father3,new ArrayList<Human>(Set.of(children3)), new HashSet<Pet>(Set.of(pet3)));


        ArrayList<Family> famList = new ArrayList<>();
        famList.add(family1);
        famList.add(family2);
        famList.add(family3);

        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao(famList);
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.createNewFamily(new Woman("mother4", "family4Surname", "01/01/2000"), new Man("father4", "family4Surname", "01/01/1998"));

        System.out.println("----------displayAllFamilies---------");
        familyController.displayAllFamilies();

        System.out.println("----------CHILDREN ADDING---------");

        System.out.println("----------FOR 1 FAMILY---------");
        System.out.println("------------bornChild---------");
        familyController.bornChild(familyController.getFamilyById(0), "Evgen", "Tetiana");
        System.out.println("------------adoptChild---------");
        familyController.adoptChild(familyController.getFamilyById(0), new Woman("Kateryna", "Ivanenko", "01/01/2003", 80));
        System.out.println("---------getFamilyById-----------");
        System.out.println(familyController.getFamilyById(0));

        System.out.println("----------FOR 2 FAMILY---------");
        System.out.println("------------bornChild---------");
        familyController.bornChild(familyController.getFamilyById(1), "Oleksiy", "Vasylyna");
        System.out.println("---------getFamilyById-----------");
        System.out.println(familyController.getFamilyById(1));

        System.out.println("----------FOR 3 FAMILY---------");
        System.out.println("------------adoptChild---------");
        familyController.adoptChild(familyController.getFamilyById(2), new Woman("Iryna", "Sydorenko", "01/01/2006", 82));
        System.out.println("---------getFamilyById-----------");
        System.out.println(familyController.getFamilyById(2));

        System.out.println("----------FOR 4 FAMILY---------");
        System.out.println("------------bornChild---------");
        familyController.bornChild(familyController.getFamilyById(3), "Volodymyr", "Olha");
        System.out.println("---------getFamilyById-----------");
        System.out.println(familyController.getFamilyById(3));

        System.out.println("----------displayAllFamilies---------");
        familyController.displayAllFamilies();

        System.out.println("----------getFamiliesBiggerThan--(4)----");
        familyController.getFamiliesBiggerThan(4);

        System.out.println("------getFamiliesLessThan-----(4)------");
        familyController.getFamiliesLessThan(4);

        System.out.println("-------countFamiliesWithMemberNumber---(4)-----");
        System.out.println(familyController.countFamiliesWithMemberNumber(4));

        System.out.println("-------------count-----------------");
        System.out.println(familyController.count());

        System.out.println("---------------deleteAllChildrenOlderThen---(18)-------");
        familyController.deleteAllChildrenOlderThen(18);
        familyController.displayAllFamilies();

        System.out.println("----------deleteFamilyByIndex----------");
        familyController.deleteFamilyByIndex(1);
        familyController.displayAllFamilies();

        System.out.println("-------------count-----------------");
        System.out.println(familyController.count());

        System.out.println("----------getPets------------");
        System.out.println(familyController.getPets(1));

        System.out.println("-----------addPet---------");
        System.out.println(familyController.getFamilyById(2).getPet());
        familyController.addPet(2, new Dog());
        System.out.println(familyController.getFamilyById(2).getPet());

        Runtime.getRuntime().gc();
    }
}
