package hw10;

import java.util.ArrayList;
import java.util.List;

public interface FamilyDao {
    public ArrayList<Family> getAllFamilies();
    public Family getFamilyByIndex(int famIndex);
    public boolean deleteFamily(int famIndex);
    public boolean deleteFamily(Family deletedFamily);
    public void saveFamily(Family updatedFamily);
}
