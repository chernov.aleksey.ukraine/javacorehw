package hw10;

import java.util.Arrays;
import java.util.Objects;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;



public class Family {
    private Human mother;
    private Human father;
    private ArrayList<Human> children = new ArrayList<>();
    private HashSet<Pet> pets;

    public Human getMother() {return mother;}
    public void setMother(Human mother) {
        this.mother = mother;
        this.mother.setFamily(this);}
    public Human getFather() {return father;}
    public void setFather( Human father) {
        this.father = father;
        this.father.setFamily(this);}
    public  ArrayList<Human> getChildren() {return children;}
    public void setChildren(ArrayList<Human> children) {this.children = children;}
    public  HashSet<Pet>  getPet() {return pets; }
    public void setPet( HashSet<Pet> pets) {this.pets = pets;}

    public Family(){}
    public Family( Human mother,  Human father) {
        this.mother = mother;
        this.father = father;
        this.father.setFamily(this);
        this.mother.setFamily(this);
    }
    public Family( Human mother,  Human father, HashSet<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.pets = pets;
        this.father.setFamily(this);
        this.mother.setFamily(this);
    }
    public Family( Human mother,  Human father,  ArrayList<Human> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.father.setFamily(this);
        this.mother.setFamily(this);
    }
    public Family(Human mother, Human father, ArrayList<Human> children, HashSet<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pets = pets;
        this.father.setFamily(this);
        this.mother.setFamily(this);
    }
    public void addChild( Human newChild) {
        this.children.add(newChild);
        newChild.setFamily(this);
    }
    public void deleteChild(Human deletedChild) {
        if (this.children.contains(deletedChild)) {
            this.children.remove(deletedChild);
        }
    }
    public void deleteNumChild(int number) {
        if(number < this.children.size()) {
            this.children.remove(children.get(number));
        }
    }
    public int countFamily() {
        return 2 + this.getChildren().size();
    }
    public String childrenToString() {
        return this.children.toString();
    }
    @Override
    public String toString() {
        String familyPet = ( this.pets == null) ? "No pets" : pets.toString();
        if (mother == null || father == null) {return "FAMILY: null";
        } else {
            return "FAMILY:\nmother: " + mother.toString() +
                    "\nfather: " + father.toString() +
                    "\nchildren: " + this.childrenToString() +
                    "\npets: " + familyPet;
        }
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = ( Family) o;
        return mother.equals(family.mother) && father.equals(family.father) && Objects.equals(children, family.children) && Objects.equals(pets, family.pets);    }
    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }


}