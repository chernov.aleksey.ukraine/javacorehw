package hw12;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

final public class Man extends Human implements java.io.Serializable{
    public Man() {
    }

    public Man(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }
    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, String birthDate, int iq, HashMap<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
    @Override
    public void greetPet(Pet pet) {
        for (Pet el : this.getFamily().getPet()) {
            if (el.equals(pet)) {
                System.out.println("Привет, " + el.getNickname() + "! Это я - твой хозяин!");
            }
        }
    }
@Override
    public void unicActivity() {
        System.out.println("Я чиню машину!");
    }
    @Override
    public String toString() {

        return "Man{name= '" + this.getName() + "', surname= '" + this.getSurname() + "', birthDate= " + this.ageStringer() +
                ", iq= " + this.getIq() + ", schedule= " + getSchedule().toString() + "}";
    }
    public void prettyFormat(){
        System.out.println(this.toString());
    }

}