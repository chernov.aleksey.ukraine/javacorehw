package hw12;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao{
    private ArrayList<Family> familyList;

    public CollectionFamilyDao(ArrayList<Family> familyList) {
        this.familyList = familyList;
    }
    public void setFamilyList(ArrayList<Family> familyList) {this.familyList = familyList;}

    @Override
    public ArrayList<Family> getAllFamilies() {
        return this.familyList;
    }

    @Override
    public Family getFamilyByIndex(int famIndex) {
        return this.familyList.get(famIndex);
    };

    @Override
    public boolean deleteFamily(int famIndex) {
        boolean isDeleted;
        if (famIndex >= this.familyList.size()||famIndex<0) {
            System.out.println("Sorry, there isn't family with index " + famIndex);
            return false;
        }
        this.familyList.remove(famIndex);
        System.out.println("Done");
        return true;
    };

    @Override
    public boolean deleteFamily(Family deletedFamily) {
        return this.familyList.remove(deletedFamily);
    };

    @Override
    public void saveFamily(Family updatedFamily) {
        this.familyList.remove(updatedFamily);
        this.familyList.add(updatedFamily);
    };

    @Override
    public void loadData(ArrayList<Family> families) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("data"))) {
            objectOutputStream.writeObject(families);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void downloadData() {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("data"))) {
            ArrayList<Family> families = (ArrayList<Family>) objectInputStream.readObject();
            this.setFamilyList(families);
        } catch (IOException e) {
            System.err.println("Не удолось найти сохраненные данные");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
