package hw12;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

public class Fish extends Pet implements java.io.Serializable{

    public Fish() {
        this.setSpecies(Species.FISH);
       }
    public Fish(String nickname, int age, int trickLevel, HashSet<String> habits ) {
        super(nickname, age, habits);
        this.setSpecies(Species.FISH);
        this.setTrickLevel(trickLevel);
    }


    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }

}
