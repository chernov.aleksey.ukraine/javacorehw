package hw12;

import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao(new ArrayList<>());
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);
        familyController.menu();

        Runtime.getRuntime().gc();
    }
}
